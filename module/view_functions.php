<?php

namespace views;

function debug($arr) {
    echo '<pre style="color: #FFF;">';
        print_r($arr);
    echo '</pre>';
}

function cutTitleStiring($title) : string {
    if (mb_strlen($title) > 14) {
        return $title = mb_substr($title, 0, 11) . '...';
    } else {
        return $title;
    }
}

function cmpAsc($a, $b) {
	return $a['sort'] > $b['sort'];
}

function cmpDesc($a, $b) {
	return $a['sort'] < $b['sort'];
}

function getMainMenu($arMenu, $sort = 'cmpAsc') {

	usort($arMenu, $sort);

    $activePath = array_search($_SERVER['REQUEST_URI'], array_column($arMenu, 'path'));

    if ($activePath !== false) {
         $arMenu[$activePath]['active'] = true;
         $arMenu[$activePath]['title'] = cutTitleStiring($arMenu[$activePath]['title']);
    }

    include SITE_PATH . SITE_PATH_TEMPLATE . '/menu.php';
}
