<?php 

namespace pages;

function getCurrentPage($pages) : array { 

    $currentPage = array_search(
        parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), 
        array_column($pages, 'path')
    );
    
    if ($currentPage === false) { 
        http_response_code (404);
        return array('title' => 'Страница не существует');
    }
    
    return $pages[$currentPage];
}