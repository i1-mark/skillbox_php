<?php 

$sections = [
    ['title' => 'Главная', 'path' => '/', 'sort' => 0],
    ['title' => 'О нас', 'path' => '/route/about/', 'sort' => 2],
    ['title' => 'Новости', 'path' => '/route/news/', 'sort' => 1],
    ['title' => 'Возможности', 'path' => '/route/cools', 'sort' => 3],
    ['title' => 'Обратная связь', 'path' => '/route/feedback/', 'sort' => 4]
];
