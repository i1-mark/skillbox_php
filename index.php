<?php
error_reporting(E_ALL);

require_once($_SERVER['DOCUMENT_ROOT'] . '/core/init.php');
require_once(SITE_PATH . "/controller/authorize.php");
?>
<?php require_once(SITE_PATH . SITE_PATH_TEMPLATE . '/header.php'); ?>
                    
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="left-collum-index">
                        <h1>Возможности проекта —</h1>
                        <p>Вести свои личные списки, например покупки в магазине, цели, задачи и многое другое. Делится списками с друзьями и просматривать списки друзей.</p>
                    </td>
                    <td class="right-collum-index">    
                        <div class="project-folders-menu">
                            <ul class="project-folders-v">
                            <?php if ($isAuthorized): ?>
                                <li class="project-folders-v-active"><a href="#">Авторизация</a></li>
                            <?php else: ?>
                                <li><a href="#">Регистрация</a></li>
                                <li><a href="#">Забыли пароль?</a></li>
                            <?php endif; ?>
                            </ul>
                        <div style="clear: both;"></div>
                        </div>
                        <?php 
                            if (isset($info)) {
                                require_once($info);
                            }
                        ?>
                        <?php if (!$isAuthorized): ?>
                        <div class="index-auth">
                            <form method="POST" action="<?= $_SERVER["PHP_SELF"] ?>">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td class="iat">Ваш e-mail: <br /> <input id="login" size="30" type="email" name="login" value="<?= $_POST["login"] ?? "" ?>" /></td>
                                    </tr>
                                    <tr>
                                        <td class="iat">Ваш пароль: <br /> <input id="password" type="password" size="30" name="password" value="<?= $_POST["password"] ?? "" ?>" /></td>
                                    </tr>
                                    <tr>
                                        <td><input type="submit" name="auth" value="Войти" /></td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <?php endif; ?>          
                    </td>
                </tr>
            </table>
<?php require_once(SITE_PATH . SITE_PATH_TEMPLATE . '/footer.php'); ?>