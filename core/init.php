<?php

define('SITE_PATH', $_SERVER['DOCUMENT_ROOT']);

define('SITE_PATH_TEMPLATE', '/template');

require_once(SITE_PATH . '/module/page_functions.php');

require_once(SITE_PATH . '/module/view_functions.php');

require_once(SITE_PATH . '/include/main_menu.php');

$page = pages\getCurrentPage($sections);