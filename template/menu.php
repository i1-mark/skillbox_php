<ul>
    <?php foreach ($arMenu as $menuItem): ?>
        <li><a href="<?= $menuItem['path'] ?>" <?= isset($menuItem['active']) ? "class='active'" : "" ?>><?= $menuItem['title'] ?></a></li>
    <?php endforeach; ?>
</ul>