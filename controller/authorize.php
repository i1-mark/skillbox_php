<?php

$files = ['users.php', 'passwords.php'];
$isAuthorized = false;
    
if (!empty($_POST['auth']) && !empty($_POST['login']) && !empty($_POST['password'])) {
    
    foreach ($files as $file) {
        require_once(SITE_PATH . "/include/" . $file);
    }
    
    $login = array_search($_POST['login'], $users);

    $isAuthorized = $login !== false && $passwords[$login] === $_POST['password'] ? true : false;
    
    if ($isAuthorized) {
        if (file_exists(SITE_PATH . "/include/success.php")) {
            $info = SITE_PATH . "/include/success.php";
        }
        $form = false;
    } else {
        if (file_exists(SITE_PATH . "/include/error.php")) {
            $info = SITE_PATH ."/include/error.php";    
        }
    }
}