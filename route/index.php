<?php
error_reporting(E_ALL);
require_once($_SERVER['DOCUMENT_ROOT'] . '/core/init.php');
?>
<?php require_once(SITE_PATH . SITE_PATH_TEMPLATE . '/header.php');?>

            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="left-collum-index">
                        <h1><?=$page['title']?></h1>
                        <p><?=$page['description'] ?? ''?></p>
                    </td>
                </tr>
            </table>

<?php require_once(SITE_PATH . SITE_PATH_TEMPLATE . '/footer.php');?>